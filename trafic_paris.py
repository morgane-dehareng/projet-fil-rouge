#!/opt/conda/bin/python
# -*- coding: utf-8 -*-
import sys


from datalake import datalake
from nettoyage import nettoyage
from learning import learning


def main(argv):

    if len(argv) == 0:
        print('trafic_paris.py -[OPTION]\n'
              'Options possibles:\n'
              '-datalake : Creer le datalake\n'
              '-nettoyage : Nettoyer les donnees\n'
              '-learning : Entrainer les algorithmes des machine learning\n'
              '-help : Montrer cette aide')
        sys.exit(2)

    for arg in argv:
        if arg in ("-datalake"):
            print('Je vais creer le datalake')
            fonc = datalake()
        elif arg in ("-nettoyage"):
            print('Je vais nettoyer les donnees')
            fonc = nettoyage()
        elif arg in ("-learning"):
            print('Je vais entreiner des algorithmes de machine learning')
            fonc = learning()
        else:
            print('trafic_paris.py -[OPTION]\n'
                  'Options possibles:\n'
                  '-datalake : Creer le datalake\n'
                  '-nettoyage : Nettoyer les donnees\n'
                  '-learning : Entrainer les algorithmes des machine learning\n'
                  '-help : Montrer cette aide')
            sys.exit()


if __name__ == "__main__":
    main(sys.argv[1:])
