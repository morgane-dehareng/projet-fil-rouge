#!/opt/conda/bin/python
# -*- coding: utf-8 -*-
"""
Created on Sat May  9 15:26:02 2020

@author: pcumani
"""
from pyspark.sql import SparkSession
from web import Web


class GeoApiAdresse(Web):
    """ encapsulation de API : geo.api.gouv.fr/adresse
    pour obtenir le nom de la rue à partir d'une longitude et latitude
    """
    def __init__(self, lon=0.0, lat=0.0):
        """Constructeur API adresse  """
        Web.__init__(self)
        self._root_api = "https://api-adresse.data.gouv.fr/reverse/"
        self.set_lon(lon)
        self.set_lat(lat)
        self.get_adresse()

    def set_lon(self, lon):
        """ Setter longitude """
        if not isinstance(lon, float):

            raise Exception("ERREUR: Le TYPE de la longitude doit "
                            "être un float {LON}".format(LON=lon))
        self.lon = lon

    def set_lat(self, lat):
        """ Setter latitude """
        if not isinstance(lat, float):

            raise Exception("ERREUR: Le TYPE de la latitude doit "
                            "être un loat {LAT}".format(LAT=lat))
        self.lat = lat

    def get_lon(self):
        """ getter longitude """
        return self.lon

    def get_lat(self):
        """ getter latitude """
        return self.lat

    def get_root_api(self):
        """ getter _root_api """
        return self._root_api

    def get_adresse(self):
        """ methode get_adresse
        """
        app_name = "geoApiAdresse"
        master = "local[1]"
        spark = SparkSession.builder.appName(app_name).master(master).getOrCreate()
        sc = spark.sparkContext

        if self.get_lat() == 0.0 and self.get_lon() == 0.0:
            raise Exception("ERREUR: La longitude et la latitude sont nulles ")

        # construction de l'url pour appeler l'API
        route = "{ROOT}?lon={LON}&lat={LAT}" \
                .format(ROOT=self.get_root_api(),
                        LON=self.get_lon(),
                        LAT=self.get_lat())
        # Appelle de l'API
        response = self.call_api(route, type_response="json")

        # Chargement de la répose dans une dataFrame
        s = spark.read.json(sc.parallelize([response]))

        adresse_df.createOrReplaceTempView("apiAdresse")
        self.adresse_df = spark.sql("select * from apiAdresse")
        return self.adresse_df

    def get_adresseDF(self):
        """ methode get street from adre
        """
        return self.adresse_df

    def get_street(self):
        """ methode get street from adress dataframe
        """
        return self.adresse_df.select("features.properties.street").collect()[0][0]
