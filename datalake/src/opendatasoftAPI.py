#!/opt/conda/bin/python
# -*- coding: utf-8 -*-
"""
Definition de la classe OpenDataSoft pour telecharger
de document en utilsant API v2
"""

from web import Web


class OpenDataSoft(Web):
    """ Classe API v2 OPENDATASOFT
    """

    def __init__(self, datasetid, hdfs_path=""):
        if not hdfs_path:
            Web.__init__(self)
        else:
            Web.__init__(self, hdfs_path=hdfs_path)
        self.set_datasetid(datasetid)
        self._root_api = "https://data.opendatasoft.com/api/v2/opendatasoft/datasets/"
        self._keyword_fields = "fields"
        self._fields = []

    def set_datasetid(self, datasetid):
        """Setter datasetid
        """
        self.datasetid = datasetid

    def get_datasetid(self):
        """Getter datasetid
        """
        return self.datasetid

    def get_root(self):
        """Getter Root
        """
        return self._root_api

    def set_root(self, new_root):
        """Setter Root
        """
        self._root_api = new_root

    def get_fields(self):
        """Acquisition de noms des champs du jeu de données
        """

        route = "{ROOT}{DATASETID}".format(ROOT=self.get_root(),
                                           DATASETID=self.get_datasetid())
        response = self.call_api(route)
        if response is None:
            print("ERREUR: methode get_fields route ={} erreur lors de l'appel"
                  "API opendatasoft".format(route))
            return None
        if self._keyword_fields not in response.keys():
            print("ERREUR: methode get_fields route ={} le champ"
                  " 'fields' NON trouvé" .format(route))
            return None
        return response[self._keyword_fields]

    def set_fields(self, new_fields):
        """Setter fields
        """
        if new_fields is None:
            raise Exception("ERREUR: methode set_fields erreur lors de l'appel"
                            "methode get_fields")
        self._fields = new_fields

    def exports_datas(self, rows=-1, file_type="json", refine=[]):
        """Acquisition des données du jeu de données
        """
        filter = ''
        if len(refine) > 0:
            filter = 'where='
            for x in refine:
                filter = filter+x+'%20and%20'
            filter = filter[:-9]

        route = ("{ROOT}{DATASETID}/exports"
                 "/{file_type}?{filter}&rows={rows}&pretty=true&timezone=UTC"
                 .format(ROOT=self.get_root(),
                         DATASETID=self.get_datasetid(),
                         file_type=file_type,
                         rows=rows,
                         filter=filter))

        print(route)
        response = self.call_api(route, type_response=file_type)
        if response is None:
            raise Exception("ERREUR: methode exports_datas route ={} erreur "
                            "lors de l'appel API opendatasoft".format(route))
        return response

    def exports_hdfs(self, rows=-1, file_type="json", refine=''):
        """Acquisition des données du jeu de données et stockage en hdfs
        """
        filter = ''
        if len(refine) > 0:
            filter = 'where='
            for x in refine:
                filter = filter+x+'%20and%20'
            filter = filter[:-9]

        route = ("{ROOT}{DATASETID}/exports"
                 "/{file_type}?{filter}&rows={rows}&pretty=true&timezone=UTC"
                 .format(ROOT=self.get_root(),
                         DATASETID=self.get_datasetid(),
                         file_type=file_type,
                         rows=rows,
                         filter=filter))

        print(route)
        self.set_url(route)
        response = self.store_hdfs(dir_save="/user/root/", file_type=file_type)
