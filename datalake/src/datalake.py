#!/opt/conda/bin/python
# -*- coding: utf-8 -*-
"""
Script pour la creation du datalake en HDFS
"""

import json
from opendatasoftAPI import OpenDataSoft
from web import Web


def datalake():
    # Comptage routiere
    dataset = "comptages-routiers-permanents@parisdata"
    # Pour l'utiliser en local, Controler l'IP de la machine cloudera
    # Res = OpenDataSoft(dataset, hdfs_path="hdfs://172.18.0.2:8020")
    Res = OpenDataSoft(dataset)
    # libelle = ['Pyrenees', 'Tolbiac']
    libelle = []
    # Boucle sur mois (jusqu'a aout 2020) et libelle si specifie
    for month in range(1, 8):
        if len(libelle) == 0:
            # Changer le valeur rows pour telecharger plus ou moins données. -1 = tous
            datas = Res.exports_hdfs(rows=-1, refine=["t_1h>=date'2020-{:02d}-01'".format(month),
                                                      "t_1h<date'2020-{:02d}-01'".format(month+1)],
                                     file_type='csv')

        else:
            for lab in libelle:
                datas = Res.exports_hdfs(rows=-1, refine=["t_1h>=date'2020-{:02d}-01'".format(month),
                                                          "t_1h<date'2020-{:02d}-01'".format(month+1),
                                                          'libelle="{}"'.format(lab)],
                                         file_type='csv')

    # referentiel geographique
    dataset = "referentiel-comptages-routiers@parisdata"
    Res.set_datasetid(dataset)
    datas = Res.exports_hdfs(rows=-1)

    # chantiers
    # Res.set_root("https://data.iledefrance.fr/api/v2/catalog/datasets/")
    dataset = "chantiers-perturbants@parisdata"
    Res.set_datasetid(dataset)
    datas = Res.exports_hdfs(rows=-1, file_type='csv')

    del Res

    annees = ['2014', '2015', '2016', '2017', '2018', '2019']

    # Page = Web(hdfs_path="hdfs://172.18.0.2:8020")
    Page = Web()

    for annee in annees:
        # Historique trafic
        url = "https://opendata.paris.fr/api/datasets/1.0/comptages-routiers-"\
              "permanents-historique/attachments/opendata_txt_%s_zip/" % (annee)

        Page.set_url(url)
        Page.store_hdfs(file_type="zip")

        # jours feries
        zone = 'metropole'

        url = "https://calendrier.api.gouv.fr/jours-feries/%s/%s.json" % (zone, annee)

        Page.set_url(url)
        Page.store_hdfs(file_type="json")

        # Meteo
        url = 'https://www.historique-meteo.net/site/'\
              'export.php?ville_id=188&annee=%s' % (annee)
        Page.set_url(url)
        Page.store_hdfs(file_type="csv")

    # jours feries 2020
    zone = 'metropole'
    url = "https://calendrier.api.gouv.fr/jours-feries/%s/2020.json" % (zone)
    Page.set_url(url)
    Page.store_hdfs(file_type="json")

    # Meteo 2020
    url = 'https://www.historique-meteo.net/site/'\
          'export.php?ville_id=188&annee=2020'
    Page.set_url(url)
    Page.store_hdfs(file_type="csv")


if __name__ == "__main__":
    datalake()
