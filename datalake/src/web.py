#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Definition de la classe Web pour la gestion du
telechargement des fichiers et page web
"""

import json
import requests
import re
from urllib.error import HTTPError
from io import BytesIO
from zipfile import ZipFile
from pyspark.sql import SparkSession, utils


class Web():
    """Classe WEB
    """
    def __init__(self, url="", hdfs_path="hdfs://pfr-cloudera:8020"):
        self.set_url(url)
        self.set_hdfs_path(hdfs_path)

    def set_url(self, url):
        """ URL Setter
        """
        self.url = url

    def set_hdfs_path(self, path):
        """ HDFS Path Setter
        """
        self.hdfs_path = path

    def get_url(self):
        """ URL getter
        """
        return self.url

    def get_hdfs_path(self):
        """ HDFS Path Getter
        """
        return self.hdfs_path

    def get_page(self):
        """ Get page content
        """
        try:
            response = requests.get(self.get_url())
            page = response.content
        except HTTPError:
            raise Exception("ERROR HTTP :", HTTPError)
        return page

    def download_page(self, dir_save="./", headers=None, data=None, file_type=""):
        """ Download page or file
        """

        response_api = self.call_api(self.url, headers=headers, data=data, type_response=file_type)

        filename = re.sub("http[s]?://(www.)?", "", self.url)
        filename = re.sub("[?&=/.]", "-", filename)

        if file_type == "":
            with open(dir_save+'/'+filename+'.html', 'w') as fout:
                fout.write(response_api)

        elif file_type == "json":
            with open(filename+'.json', 'w') as fout:
                json.dump(response_api, fout)

        elif file_type == "csv":
            with open(dir_save+'/'+filename+'.csv', 'w') as fout:
                fout.write(response_api)

        elif file_type == "zip":
            with ZipFile(BytesIO(response_api)) as my_zip_file:
                my_zip_file.extractall(dir_save)

    def store_hdfs(self, dir_save="/user/root/", headers=None, data=None, file_type=""):
        """ Store page or file in HDFS
        """

        response_api = self.call_api(self.url, headers=headers, data=data, type_response=file_type)

        filename = re.sub("http[s]?://(www.)?", "", self.url)
        filename = re.sub(r"\W+", "-", filename)

        spark = SparkSession.builder.appName('store')\
                                    .master('local[6]')\
                                    .config("spark.driver.memory", "15g")\
                                    .config("spark.executor.memory", "15g")\
                                    .getOrCreate()
        sc = spark.sparkContext

        if file_type == "":
            # TODO A implementer
            raise TypeError('Sauvegarde en HDFS de fichier html encore pas implemente')

        elif file_type == "json":

            # Chargement de la répose dans une dataFrame
            spark_df = spark.read.json(sc.parallelize([response_api]))

            # Create temp view pour
            spark_df.createOrReplaceTempView("api_temp")
            spark_df = spark.sql("select * from api_temp")
            try:
                spark_df.write.parquet(self.hdfs_path+dir_save+filename)
            except utils.AnalysisException:
                print("Le fichier {} existe deja en HDFS".format(self.hdfs_path+dir_save+filename))

        elif file_type == "csv":

            if '\r\n' in response_api:
                resp = sc.parallelize(response_api.rstrip().split("\r\n"))
            elif '\n' in response_api:
                resp = sc.parallelize(response_api.rstrip().split("\n"))
            else:
                resp = sc.parallelize(response_api.rstrip())

            # Controler le type de separator
            if any([';' in x for x in resp.take(4)]):
                spark_df = spark.read.option("sep", ";")\
                                 .option("header", "true")\
                                 .option("comment", "#").csv(resp)
            else:
                spark_df = spark.read.option("sep", ",")\
                                 .option("header", "true")\
                                 .option("comment", "#").csv(resp)

            try:
                spark_df.write.parquet(self.hdfs_path+dir_save+filename)
            except utils.AnalysisException:
                print("Le fichier {} existe deja en HDFS".format(self.hdfs_path+dir_save+filename))

        elif file_type == "zip":

            with ZipFile(BytesIO(response_api)) as my_zip_file:
                # Boucle sur les fichiers dans le .zip
                for subfile in my_zip_file.namelist():

                    rdd = sc.parallelize(my_zip_file.read(subfile)
                                                    .decode("utf-8")
                                                    .rstrip()
                                                    .split("\n"))

                    spark_df = spark.read.option("sep", ";")\
                        .option("header", "true")\
                        .option("comment", "#").csv(rdd)
                    try:
                        spark_df.write.parquet(self.hdfs_path+dir_save+subfile)
                    except utils.AnalysisException:
                        print("Le fichier {} existe deja en HDFS".format(
                              self.hdfs_path+dir_save+subfile))
        sc.stop()

    def call_api(self, url, headers=None, data=None, type_response="json"):
        """ Call API
        """

        try:
            response = requests.get(url)  # requests.request("GET", url, headers=headers, data=data)
        except HTTPError:
            raise Exception("ERROR HTTP :", HTTPError)
        response_api = response.text
        if type_response == "json":
            try:
                response_api = json.loads(response_api)
            except Exception:
                msg = 'The file you are trying to download from {} is not a JSON'.format(self.url)
                raise TypeError(msg)
        elif type_response == 'zip':
            response_api = response.content

        return response_api
