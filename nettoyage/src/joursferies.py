#!/opt/conda/bin/python
# -*- coding: utf-8 -*-

import datetime
from managehdfs import managehdfs


class Joursferies(managehdfs):
    def __init__(self, basedir, hdfs_path="hdfs://pfr-cloudera:8020"):
        self.set_hdfs_path(hdfs_path)
        self.init_spark()
        self.joursferies = []
        self.basedir = basedir

    def get_days(self):
        if len(self.joursferies) == 0:
            self.set_days()
        return self.joursferies

    def set_days(self):
        for year in range(2014, 2021):
            dir = self.basedir+"{}*".format(year)
            self.joursferies = self.joursferies + (self.spark.read
                                                   .parquet(self.hdfs_path+dir).columns)

        self.joursferies = [datetime.datetime.strptime(x, '%Y-%m-%d').date()
                            for x in self.joursferies]
