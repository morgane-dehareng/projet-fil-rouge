#!/opt/conda/bin/python
# -*- coding: utf-8 -*-
import geopandas
from geopandas import GeoSeries
from shapely.geometry import shape
from shapely.geometry import Polygon

from pyspark.sql.types import IntegerType
from pyspark.sql import functions as F

import json

from chantier import Chantier
from chantier import Geoshape
from referentiel import referentiel_geographique
from managehdfs import managehdfs


class FichierCapteurChantier(managehdfs):

    def __init__(self, df_input, hdfs_path="hdfs://pfr-cloudera:8020"):
        self.df_input = df_input
        self.set_dataframe(df_input)
        self.colonnes = [col for col in self.df_input.columns if col != "identifiant"]
        self.set_hdfs_path(hdfs_path)
        self.init_spark()

    def corriger_chevauchements(self):
        df_input = self.df_input
        self.init_controle()

        df_chevauchements = self.identifier_chevauchements()
        if df_chevauchements.count() == 0:
            print("aucun chevauchement")
            return self.df_input.select(self.colonnes)

        df_corrections = self.recalculer_periodes(df_chevauchements)

        df_output = self.construire_dataframe_corrigee(df_chevauchements, df_corrections)

        return df_output

    def get_dataframe(self):
        return self.df

    def set_dataframe(self, df_relation):
        self.df = df_relation
        self.df.createOrReplaceTempView("vue_trafic_chantier")

    def get_poids_impact_circulation(self):
        liste = [('BARRAGE_TOTAL', 5),
                 ('IMPASSE', 4),
                 ('SENS_UNIQUE', 3),
                 ('RESTREINTE', 2),
                 ('Mise en impasse', 1)]
        return self.spark.createDataFrame(liste, ["impact_circulation", "poids"]).coalesce(1)

    def init_controle(self):
        requeteSQL = """select distinct vue_trafic_chantier.*,
                             row_number() over(order by iu_ac,  date_debut, date_fin) id,
                             count(*) over(partition by iu_ac order by iu_ac, date_debut, date_fin) nombre_chantiers,
                             row_number() over(partition by iu_ac order by iu_ac, date_debut, date_fin) rang_chantier
                     from vue_trafic_chantier
                 """
        df_controle = self.spark.sql(requeteSQL)
        self.set_dataframe(df_controle)

    def recalculer_periodes(self, df):

        # ajouter le poids de l'impact : dans le cas ou  une période  comporte
        #  plusieurs impacts, le plus lourd  d'entre eux sera sélectionné
        df.createOrReplaceTempView("chevauchement")
        df_chevauchements = self.add_poisds_impact(df)
        df_chevauchements.createOrReplaceTempView("chevauchement")

        #  Construire tous les points des intervals
        df_all_points = self.construct_all_points(df_chevauchements)

        # Contruire les intervals
        df_intervals = self.construire_intervals(df_all_points, df_chevauchements)

        # Recalculer les périodes
        df_intervals.createOrReplaceTempView("intervals")
        data_corrigees = self.spark.sql("""
                             select distinct a.iu_ac,
                                     a.date_debut,
                                     a.date_fin,
                                     max(b.niveau_perturbation) over(partition by a.iu_ac, a.date_debut, a.date_fin)
                                         niveau_perturbation,
                                     max(b.poids) over(partition by a.iu_ac, a.date_debut, a.date_fin)
                                         as poids
                             from intervals a inner join chevauchement b
                                  on (a.iu_ac = b.iu_ac and
                                      a.date_debut >= b.date_debut
                                      and a.date_fin <= b.date_fin)
                               """).coalesce(1)

        data_corrigees.createOrReplaceTempView("data_corrigees")
        resultat = self.spark.sql("""select distinct a.*, b.impact_circulation
                                       from data_corrigees a inner join poids_impact b
                                       on (a.poids = b.poids)
                                          """).coalesce(1)
        self.spark.catalog.dropTempView("data_corrigees")
        self.spark.catalog.dropTempView("intervals")
        self.spark.catalog.dropTempView("points")
        self.spark.catalog.dropTempView("poids_impact")

        return resultat

    def construire_dataframe_corrigee(self, df_chevauchements, df_corrections):
        colonnes = self.colonnes

        df = self.get_dataframe()
        id_rows = df_chevauchements.select('id').distinct().coalesce(4).collect()
        ids = list(map(lambda x: x.id, id_rows))
        df_good = df.filter(~df['id'].isin(ids)).select(colonnes)
        df_output = df_good.union(df_corrections.select(colonnes))
        return df_output

    def construire_intervals(self, df_all_points, df_chevauchements):
        df_all_points.createOrReplaceTempView("points")
        intervals = self.spark.sql("""select iu_ac,
                                          point date_debut,
                                          lead(point,1) over(partition by iu_ac order by point) date_fin,
                                          row_number() over(partition by iu_ac order by  point ) numero_ordre_date,
                                          (count(*) over(partition by iu_ac)) nombre_dates
                                      from points
                                      order by iu_ac, point
                                """).coalesce(1)
        intervals.createOrReplaceTempView("intervals")

        df_resultat = self.spark.sql("""select iu_ac,
                                         date_debut,
                                         case when numero_ordre_date == nombre_dates then
                                              date_debut
                                         else
                                             date_sub(date_fin,1)
                                         end date_fin,
                                         numero_ordre_date,
                                         nombre_dates
                                   from intervals

                                  """).coalesce(1)
        return df_resultat

    def identifier_chevauchements(self):
        df_chevauchements = self.spark.sql("""
                                    SELECT *
                                    FROM vue_trafic_chantier
                                    WHERE iu_ac in (
                                        SELECT distinct  a.iu_ac
                                        FROM vue_trafic_chantier a
                                             INNER join  vue_trafic_chantier b
                                                   on(a.iu_ac = b.iu_ac
                                                      and a.id <> b.id
                                                      and b.date_fin >= a.date_debut
                                                      and b.date_debut <= a.date_fin
                                                     ))
                                     """).coalesce(1)

        return df_chevauchements

    def construct_all_points(self, df):
        df.createOrReplaceTempView("chevauchement")

        requete_sql = """
             select distinct c.iu_ac ,
                    c.point,
                    coalesce(c.point_suivant,c.point) point_suivant
              from( select distinct b.iu_ac,
                           b.point,
                          lead(b.point,1) over(partition by b.iu_ac order by b.point) point_suivant
                    from (select iu_ac, date_debut point
                                 from chevauchement
                           union
                           select  iu_ac, date_fin point
                                   from chevauchement
                          ) b
                    ) c
                  order by 1,2
                 """
        df_points_sans_etats = self.spark.sql(requete_sql).coalesce(1)
        df_points_sans_etats.createOrReplaceTempView("points_sans_etat")
        requete_sql = """
               select distinct  c.iu_ac,
                      date_format(c.point,"yyyy-MM-dd") point,
                       date_format(c.point_suivant,"yyyy-MM-dd") point_suivant,
                      count(etat) over(partition by c.iu_ac, c.point, c.point_suivant) nombre_etats
              from (
                    select distinct  a.iu_ac, a.point, a.point_suivant,
                           concat(b.niveau_perturbation, b.impact_circulation) etat
                    from points_sans_etat a left outer join  chevauchement b
                         on(a.iu_ac = b.iu_ac
                          and a.point_suivant > b.date_debut
                          and a.point <= b.date_fin )
                    ) c
                """
        df_points_etats = self.spark.sql(requete_sql).coalesce(1)

        df_resultat = df_points_etats.filter(df_points_etats.nombre_etats == 1)\
            .withColumn("point", F.to_date(F.col("point")))\
            .select(["iu_ac", "point"])
        df_points_etats_multiples = df_points_etats.filter(df_points_etats.nombre_etats > 1)

        for ligne in df_points_etats_multiples.collect():
            iu_ac = ligne.iu_ac
            debut = ligne.point
            fin = ligne.point_suivant

            requete_sql = """SELECT '{IU_AC}' iu_ac,
                                  array(to_date('{DEBUT}','yyyy-MM-dd'),
                                       date_add(to_date('{DEBUT}','yyyy-MM-dd'),1),
                                       to_date('{FIN}','yyyy-MM-dd'),
                                       date_add(to_date('{FIN}','yyyy-MM-dd'),1)
                                         ) as point
                         """\
                         .format(IU_AC=iu_ac, DEBUT=debut, FIN=fin)

            df_points = self.spark.sql(requete_sql)\
                .withColumn("point", F.explode(F.col("point")))

            df_union = df_resultat.union(df_points)
            df_resultat = df_union

        return df_resultat.distinct().coalesce(1)

    def add_poisds_impact(self, df):
        df_chevauchements = df

        self.get_poids_impact_circulation().createOrReplaceTempView("poids_impact")
        df_reultat = self.spark.sql("""select a.*, b.poids
                                   from chevauchement a left outer join poids_impact b
                                        on (a.impact_circulation = b.impact_circulation)
                               """).coalesce(1)

        return df_reultat


class Jointures(managehdfs):
    """Class pour unifier les differents
    base des données du datalake apres nettoyage"""

    @staticmethod
    def findChantier(arc, shapeARC, id_chantier, shape_chantier):

        zone = shape(json.loads(shapeARC))
        series = geopandas.GeoSeries(zone)
        df_geopandas_ARC = geopandas.GeoDataFrame({'geometry': series,
                                                   'init': 'epsg:2154'})
        zone1 = shape(json.loads(shape_chantier))
        series1 = geopandas.GeoSeries(zone1)
        df_geopandas_chantier = geopandas.GeoDataFrame({'geometry': series1,
                                                        'init': 'epsg:2154'})
        if type(df_geopandas_chantier) == geopandas.geodataframe.GeoDataFrame and \
                type(df_geopandas_ARC) == geopandas.geodataframe.GeoDataFrame:

            res_intersection = geopandas.overlay(df_geopandas_ARC,
                                                 df_geopandas_chantier, how='intersection')
            if int(res_intersection.count()[0]) > 0:
                return True

        return False

    def __init__(self, hdfs_path="hdfs://pfr-cloudera:8020", filepath=""):
        self.set_hdfs_path(hdfs_path)
        self.init_spark()
        self.sc.addPyFile(filepath+"jointures.py")
        self.sc.addPyFile(filepath+"chantier.py")
        self.sc.addPyFile(filepath+"referentiel.py")
        self.sc.addPyFile(filepath+"managehdfs.py")

    def chantier_referentiel(self, objChantier, df_chantier, df_arcs):
        """Créer la relation entre les chantiers et les capteurs
        """

        objetGeoshape = Geoshape()

        nb = df_chantier.count()
        rdd_geopandas_Chantier = objetGeoshape.build_geopandas_rdd(df=df_chantier,
                                                                   ligne_debut=0,
                                                                   nombre_lignes=nb)
        rdd_geopandas_Chantier.cache()
        # Calculer les intersections chantiers/arc

        nombre_iterations = 5
        nombre_lignes = int(df_arcs.count()/nombre_iterations)-1
        ligne_debut = 0
        for i in range(0, nombre_iterations):
            if i > 0:
                ligne_debut += nombre_lignes + 1

            if i == (nombre_iterations - 1):
                nombre_lignes = df_arcs.count() - (i)*nombre_lignes
            rdd_geopandas_ARCS = objetGeoshape.build_geopandas_rdd(df_arcs,
                                                                   ligne_debut=ligne_debut,
                                                                   nombre_lignes=nombre_lignes)
            rddCartesien = rdd_geopandas_ARCS.cartesian(rdd_geopandas_Chantier).repartition(12)

            rdd_resultats = rddCartesien.filter(lambda x: Jointures.findChantier(x[0][0],
                                                                                 x[0][1],
                                                                                 x[1][0],
                                                                                 x[1][1])).map(
                                                                                 lambda x: (x[0][0],
                                                                                            x[1][0]))

            if not rdd_resultats.isEmpty():
                try:
                    resultats = resultats.union(rdd_resultats)
                except UnboundLocalError:
                    resultats = rdd_resultats

            else:
                print("EXCEPTION : Aucun resultat dans jointure chantiers et les capteurs")

        # Créer la table qui associe  un identifiant arc (iu_ac) à un ou plusieurs
        # identifiants de chantier suppression des variables inutiles

        dfRelationCapteurChantier = resultats.toDF(["iu_ac", "identifiant"])
        dfRelationCapteurChantier.cache()

        # VI - PRODUIRE LE FICHIER QUI MET EN RELATION LES CHANTIERS ET LES ARCS

        colonnesChantier = objChantier.get_colonnes_pertinentes()[0:-1]
        colonnesChantier.append("identifiant")
        chantier = objChantier.get_dataFrame().select(colonnesChantier)
        chantier.createOrReplaceTempView("chantier")

        dfRelationCapteurChantier.createOrReplaceTempView("relation_chantier_trafic")

        requeteSQL = """
                   SELECT b.iu_ac, a.*
                   FROM chantier as a  inner join relation_chantier_trafic as b
                        on( a.identifiant = b.identifiant)
                   """
        df_fichier = self.spark.sql(requeteSQL)

        objFichier = FichierCapteurChantier(df_fichier)
        df_fichier = objFichier.corriger_chevauchements()

        return df_fichier

    def trafic_chantier(self, df_trafic, df_chantier):
        df_chantier = df_chantier.withColumn("iu_ac", df_chantier["iu_ac"].cast(IntegerType()))
        trafchan = df_trafic.join(df_chantier, on=(df_trafic.id_arc == df_chantier.iu_ac) &
                                  (F.datediff(df_trafic.date, df_chantier.date_fin) < 0) &
                                  (F.datediff(df_trafic.date, df_chantier.date_debut) > 0),
                                  how="left")

        trafchan = trafchan.fillna({'niveau_perturbation': "0",
                                    'impact_circulation': 'OUVERT'})

        return trafchan.drop('iu_ac', 'date_debut', 'date_fin', 'identifiant')

    def meteo_trafic(self, df_meteo, df_trafic):
        old_columns = [
            "t_1h",
            "temperature_morning_c",
            "temperature_noon_c",
            "temperature_evening_c"
        ]

        new_columns_names = {
            "precip_total_day_mm": "precipitation",
            "visibility_avg_km": "visibilite",
            "cloudcover_avg_percent": "nuage",
            "total_snow_mm": "neige"
        }
        data = df_trafic.join(df_meteo, on='date', how="inner")

        data = data.withColumn("temperature",
                               F.when(data["heure"] < 10, data["temperature_morning_c"])
                               .when(data["heure"] > 16, data["temperature_evening_c"])
                               .otherwise(data["temperature_noon_c"]))
        data = data.drop(*old_columns)
        data = data.select([F.col(column).alias(new_columns_names.get(column, column))
                            for column in data.columns])
        return data

    def trafic_feries(self, trafic, days):
        # Sunday = 1, Monday = 2, ... Saturday = 7  => feries = 0
        trafic = trafic.withColumn('weekday',
                                   F.when((trafic.date).isin(days), 0)
                                   .otherwise(F.dayofweek(trafic.date)))
        trafic = trafic.drop('date', 'date_heure')
        return trafic
