#!/opt/conda/bin/python
# -*- coding: utf-8 -*-
from managehdfs import managehdfs
from chantier import Echantillion_rue


class referentiel_geographique(managehdfs):
    """
    Comptage routier - Référentiel géographique

    """

    def __init__(self, file="", hdfs_path="hdfs://pfr-cloudera:8020", df="", rues=[]):
        self.set_hdfs_path(hdfs_path)
        self.init_spark()
        if len(file) != 0:
            self.set_dataFrame(self.read_hdfs(file))
        if len(df) != 0:
            self.set_dataFrame(df)
        self.nom_vue = "referentiel_geo_arc"
        if len(rues) > 0:
            self.selection_echantillon()

    def selection_echantillon(self, rues=[]):
        """Sélectionner l'échantillion étudié"""
        df = self.get_dataFrame()
        df_echantillon = df.filter(df['libelle'].isin(rues) == True)
        self.set_dataFrame(df_echantillon)

    def construire_view_GeoShape(self):
        """ Construire la vue qui contient la geoshape de chaque arc
        """
        self.get_dataFrame().createOrReplaceTempView(self.nom_vue)
        requeteSQL = """SELECT distinct iu_ac, to_json(geo_shape.geometry) as geo_shape_json,
                               geo_point_2d.lon , geo_point_2d.lat
                        FROM {VUE} where to_json(geo_shape) is not null""".format(VUE=self.nom_vue)
        df = self.spark.sql(requeteSQL)
        self.set_dataFrame(df)
        self.get_dataFrame().createOrReplaceTempView(self.nom_vue)
        return self.nom_vue

    def export_csv(self, fileName="reverseArcs.csv",
                   noms_colonnes_entree=["lat", "lon", "iu_ac"],
                   noms_colonnes_sortie=["lat", "lon", "iu_ac"]):
        """export en csv"""
        df = self.get_dataFrame().select(noms_colonnes_entree)
        dfRenamed = df.rdd.repartition(1).toDF(noms_colonnes_sortie)
        full_file_name = os.path.join(os.getcwd(), fileName)
        dfRenamed.write \
                 .mode("overwrite") \
                 .format('com.databricks.spark.csv') \
                 .option("mapreduce.fileoutputcommitter.marksuccessfuljobs", "false")\
                 .option('header', 'true') \
                 .option("sep", ",")\
                 .csv(full_file_name)

        for f in os.listdir(path=full_file_name):
            if f[-3:] == "csv":
                name_file_spark = os.path.join(full_file_name, f)
        return name_file_spark

    def set_dataFrame(self, df):
        if df.rdd.getNumPartitions() > 12:
            new_df = df.rdd.repartition(12).toDF()
            df = new_df
            del new_df
        self.df_referentiel_geo_arc = df

    def get_dataFrame(self):
        return self.df_referentiel_geo_arc

    def set_filename_json(self, filename_json):
        """ setter nom du fichier lu
        """
        if filename_json == "":
            print("Le nom du fichier json doit être présisé - exemple"
                  " : monObjet=Referentiel_geographique_arc('./referentiel-comptage-routier.json')")
            raise Exception("ERREUR: Le nom du fichier json est manquant ")
        self.filename_json = filename_json

    def get_filename_json(self):
        """ getter nom du fichier lu
        """
        return self.filename_json

    def clean(self):
        # Construire l'échantillon des arcs étudiés
        self.selection_echantillon(Echantillion_rue().getnames())

        # Construire la vue geoshape des arcs
        self.construire_view_GeoShape()
        return self.get_dataFrame()
