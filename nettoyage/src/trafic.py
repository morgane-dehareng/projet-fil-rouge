#!/opt/conda/bin/python
# coding: utf-8

from pyspark.sql import functions as F
from pyspark.sql.types import IntegerType, StringType, FloatType, DateType


from managehdfs import managehdfs
from chantier import Echantillion_rue


class Trafic(managehdfs):
    def __init__(self, file="", hdfs_path="hdfs://pfr-cloudera:8020"):
        self.set_hdfs_path(hdfs_path)
        self.init_spark()
        if len(file) != 0:
            self.set_dataFrame(self.read_hdfs(file))

    def set_dataFrame(self, df):
        self.df_trafic = df

    def get_dataFrame(self):
        return self.df_trafic

    def rename_col(self, x):
        """Renommage des classe cibles
        """

        # df = x.drop('date_debut', 'date_fin', 'k', 'iu_nd_amont', 'libelle_nd_amont',
        #            'iu_nd_aval', 'libelle_nd_aval', 'geo_2d', 'geo_shape', 'dessin\r')
        x = x.select(['iu_ac', 'libelle', 't_1h', 'q', 'k', 'etat_trafic', 'etat_barre'])
        newColumns = ['id_arc', 'libelle', 'date_heure', 'debit_horaire', 'k', 'label', 'statut_arc']
        df = x.toDF(*newColumns)
        return df

    def dropna(self, x):
        return x.dropna()

    def mngdate(self, x):
        """parsing de la colonnes date_heure
        """
        x = x.withColumn("date", x['date_heure'].cast(DateType()))

        x = x.withColumn("annee", F.year("date_heure"))

        x = x.withColumn("mois", F.month("date_heure"))

        x = x.withColumn("dayofmonth", F.dayofmonth("date_heure"))

        x = x.withColumn("heure", F.hour("date_heure"))

        x.drop('date_heure')

        return x

    def traftype(self, x):
        """Modification des types des colonnes
        """
        traf = x.withColumn("id_arc", x["id_arc"].cast(IntegerType()))

        traf = traf.withColumn("debit_horaire", traf["debit_horaire"].cast(FloatType()))

        traf = traf.withColumn("date", traf["date"].cast(StringType()))

        return traf

    def remove_outlier(self, df):
        """Retrait d'outliers
        """
        for col in df.columns:
            if (df.schema[col].dataType == ['FloatType']):
                q1 = df.approxQuantile(col, [0.5], 0.25)
                q3 = df.approxQuantile(col, [0.5], 0.75)
                iqr = q3 - q1
                lower_bound = q1 - (1.5 * iqr)
                upper_bound = q3 + (1.5 * iqr)
                df = df[(df[col] > lower_bound) & (df[col] < upper_bound)]
        return df

    def clean(self):
        """Nettoyage donnees trafic
        """
        trafic = self.get_dataFrame()

        trafic = self.rename_col(trafic)

        # Selectionner seulement rues dans echantillons
        echantillon_rue = Echantillion_rue().getnames()
        trafic = trafic.filter(trafic['libelle'].isin(echantillon_rue))

        trafic = self.dropna(trafic)

        trafic = self.mngdate(trafic)
        trafic = self.traftype(trafic)

        return trafic
