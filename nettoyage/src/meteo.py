#!/opt/conda/bin/python
# coding: utf-8

import pyspark
from pyspark.sql import SparkSession
from pyspark.sql import functions as F

from managehdfs import managehdfs


class Meteo(managehdfs):

    def __init__(self, file="", hdfs_path="hdfs://pfr-cloudera:8020"):
        self.set_hdfs_path(hdfs_path)
        self.init_spark()
        if len(file) != 0:
            self.set_dataFrame(self.read_hdfs(file))

    def set_dataFrame(self, df):
        self.df_meteo = df

    def get_dataFrame(self):
        return self.df_meteo

    def drop_columns(self, df, columns):
        return df.drop(*columns)

    def put_columns_in_lowercase(self, df):
        return df.toDF(*(column.lower() for column in df.columns))

    def normalize_date(self, df, column, new_column, new_format):
        return df.withColumn(new_column, date_format(column, new_format))

    def get_hour(self, df, column, new_column):
        return df.withColumn(new_column, hour(column))

    def join_dataframes(self, first_df, second_df, column, join_type):
        return first_df.join(second_df, on=[column], how=join_type)

    def get_temperature(self, df, new_column, hour, morning_temp, noon_temp, evening_temp):
        return df.withColumn(new_column,
                             when(df[hour] < 10, df[morning_temp])
                             .when(df[hour] > 16, df[evening_temp])
                             .otherwise(df[noon_temp]))

    def rename_columns(self, df, columns_names):
        return df.select([col(column).alias(columns_names.get(column, column))
                          for column in df.columns])

    def clean(self):

        useless_columns = [
            "MAX_TEMPERATURE_C",
            "MIN_TEMPERATURE_C",
            "WINDSPEED_MAX_KMH",
            "HUMIDITY_MAX_PERCENT",
            "PRESSURE_MAX_MB",
            "HEATINDEX_MAX_C",
            "DEWPOINT_MAX_C",
            "WINDTEMP_MAX_C",
            "WEATHER_CODE_MORNING",
            "WEATHER_CODE_NOON",
            "WEATHER_CODE_EVENING",
            "UV_INDEX",
            "SUNHOUR",
            "OPINION"
        ]

        meteo = self.get_dataFrame()
        meteo = self.drop_columns(meteo, useless_columns)
        meteo = self.put_columns_in_lowercase(meteo)

        return meteo
