#!/opt/conda/bin/python
# -*- coding: utf-8 -*-
from chantier import Chantier
from chantier import Geoshape
from referentiel import referentiel_geographique
from managehdfs import managehdfs
from jointures import Jointures
from trafic import Trafic
from meteo import Meteo
from joursferies import Joursferies


def nettoyage():
    jointures = Jointures(filepath="/usr/src/app/nettoyage/src/")

    # Acquisition et nettoyage des données brutes du trafic historique
    print("Données trafic historique...")
    objTrafic = Trafic(file="/user/root/trafic_capteurs_20*")
    df_trafic = objTrafic.clean()

    # Acquisition et nettoyage des données brutes du chantiers
    print("Données chantiers...")
    objChantier = Chantier(file="/user/root/data-opendatasoft-com-api-v2-"
                                "opendatasoft-datasets-chantiers-perturbants-parisdata"
                                "-exports-csv-rows-1-pretty-true-timezone-UTC")
    df_chantier = objChantier.clean()

    colonnesChantier = objChantier.get_colonnes_pertinentes()[0:-1]
    colonnesChantier.append("identifiant")

    chantier = objChantier.get_dataFrame().select(colonnesChantier)

    # Acquisition et nettoyage des données brutes des referentiels
    print("Referentiels geographique...")
    objRefe = referentiel_geographique(file="/user/root/data-opendatasoft-com-api-v2-"
                                            "opendatasoft-datasets-referentiel-comptages-"
                                            "routiers-parisdata-exports-json-rows-1-pretty-"
                                            "true-timezone-UTC")

    df_arcs = objRefe.clean()

    print("Jointure chantiers/referentiels...")
    df_nettoye = jointures.chantier_referentiel(objChantier, df_chantier, df_arcs)
    del df_chantier, df_arcs, objRefe, objChantier
    # df_nettoye.show()

    print("Jointure trafic/chantier...")
    df_nettoye = jointures.trafic_chantier(df_trafic, df_nettoye)
    del df_trafic, objTrafic
    # df_nettoye.show()

    # Acquisition et nettoyage des données brutes du Meteo
    objMeteo = Meteo(file="/user/root/historique-meteo-net-site-export-php-ville_id-188-annee-*")
    df_meteo = objMeteo.clean()

    print("Jointure trafic/meteo...")
    df_nettoye = jointures.meteo_trafic(df_meteo, df_nettoye)
    del df_meteo, objMeteo
    # df_nettoye.show()

    # Acquisition et nettoyage des jours feries
    df_feries = Joursferies(basedir="/user/root/calendrier-api-gouv-fr-jours-feries-metropole-")
    holidays = df_feries.get_days()

    print("Jointure trafic/jours feries...")
    df_nettoye = jointures.trafic_feries(df_nettoye, holidays)
    df_nettoye.show()

    jointures.write_hdfs(df_nettoye, "/user/root/Nettoye")


if __name__ == "__main__":
    nettoyage()
