#!/opt/conda/bin/python
# -*- coding: utf-8 -*-
import pyspark.sql.functions as F
from pyspark.sql import SQLContext
import re

from managehdfs import managehdfs


class Geoshape(managehdfs):
    """Class to build Geopandas RDD"""
    def __init__(self, hdfs_path="hdfs://pfr-cloudera:8020"):
        self.set_hdfs_path(hdfs_path)
        self.init_spark()
        self.sqlContext = SQLContext(self.sc)

    def build_geopandas_rdd(self, df, ligne_debut=0, nombre_lignes=100):
        ligne_fin = (ligne_debut + nombre_lignes)-1

        my_rdd = df.rdd.zipWithIndex() \
                   .filter(lambda x: x[1] >= ligne_debut and x[1] <= ligne_fin) \
                   .map(lambda x: (x[0][0], x[0][1], x[1]))

        return my_rdd


class Echantillion_rue():
    def __init__(self):
        pass

    def getnames(self):
        return ['Bd_Ney', 'Pyrenees', 'Tolbiac', 'Bd_Raspail', 'Bd_Voltaire',
                'Bd_St_Germain', 'Av_Daumesnil', 'Bd_Magenta', 'Bd_Malesherbes',
                'Bd_Macdonald', 'Rivoli', 'Bd_Diderot', 'Av_du_Maine', 'Av_Jean_Jaures',
                'Bd_Haussmann', 'Av_de_Clichy', 'La_Fayette', 'Belleville', 'Convention',
                'Av_Parmentier']

    def get_criteres(self):
        criteres = []
        for rue in self.getnames():
            critere = rue
            matches = re.compile("(Av_|Bd_)(.*$)", re.IGNORECASE).search(rue)

            if matches is not None:
                critere = matches.groups()[-1]
            criteres.append("("+critere.replace('_', '?').strip()+")")

        return criteres

    def conversion(self, liste_voies=[]):
        resultat = []
        criteres = self.get_criteres()

        for voie in liste_voies:
            if voie is not None:
                for critere in criteres:
                    matches = re.compile(critere, re.IGNORECASE).search(voie)
                    if matches is not None:
                        resultat.append(voie)
                        break
        return resultat


class Chantier(managehdfs):
    """ chantiers perturbants la circulation Parisienne
    """
    def __init__(self, file="", df="", hdfs_path="hdfs://pfr-cloudera:8020"):
        self.set_hdfs_path(hdfs_path)
        self.init_spark()
        if len(file) != 0:
            self.set_dataFrame(self.read_hdfs(file))
        if len(df) != 0:
            self.set_dataFrame(df)
        self.set_colonnes_pertinentes([])
        self.set_lignes_incompletes([])
        self.nom_vue = "chantier_data_brutes"
        self.set_plan_travail_nettoyage()

    def construireViewBruteChantier(self):
        """ Construire la vue brute des chantiers
        """

        self.get_dataFrame().createOrReplaceTempView(self.nom_vue)

        requeteSQL = """SELECT a.*, geo_shape as geo_shape_json
                        FROM {VUE} as a
                        WHERE geo_shape is not null""".format(VUE=self.nom_vue)
        df = self.spark.sql(requeteSQL)
        self.set_dataFrame(df)
        self.get_dataFrame().createOrReplaceTempView(self.nom_vue)
        return self.nom_vue

    def set_dataFrame(self, df):
        df = df.withColumn('geo_shape', F.regexp_replace('geo_shape', '"\\{"', '\\{'))
        df = df.withColumn('geo_shape', F.regexp_replace('geo_shape', '\\}"', '\\}'))
        df = df.withColumn('geo_shape', F.regexp_replace('geo_shape', '""', '"'))

        self.df_chantiers_data_brutes = df

    def get_dataFrame(self):
        return self.df_chantiers_data_brutes

    def selection_echantillon(self, rues=[]):
        """Sélectionner l'échantillion étudié
        """
        df = self.get_dataFrame()
        df_echantillon = df.filter(df['voie'].isin(rues))
        self.set_dataFrame(df_echantillon)

    def get_geoshape(self):
        """"""
        return self.get_dataFrame().select('identifiant', 'geo_shape_json')

    def get_voies(self):
        """liste des voies (rues)"""
        voies = self.get_dataFrame().select('voie').distinct().collect()
        return list(map(lambda x: x.voie, voies))

    def set_colonnes_pertinentes(self, listeColonnes):
        """Set colonnes pertinentes"""
        self.colonnes_pertinentes = []

        if type(listeColonnes) != list:
            print("[set_colonnes_pertinentes] Les colonnes pertinentes doivent être de type list :")
            print("Exemple : set_colonnes_pertinentes(['date_debut', 'date_fin'])")
            raise Exception("ERREUR: La méthode set_colonnes_pertinentes attend 1 "
                            "paramètre de type list")

        if len(listeColonnes) == 0:
            return None

        if (not self.colonnes_exist(listeColonnes)):
            raise Exception("[set_colonnes_pertinentes]ERREUR: Colonne inexitante")

        self.colonnes_pertinentes = listeColonnes

    def get_colonnes_pertinentes(self):
        return self.colonnes_pertinentes

    def display_synthese_infos_manquantes(self, listeColonnes=[]):
        """ Synthèse des informations manquantes
        """
        if type(listeColonnes) != list:
            print("[display_synthese_data] La méthode attend 1 paramètre de type list ")
            raise Exception("ERREUR: La méthode attend 1 paramètre de type list")

        if len(listeColonnes) == 0:
            listeColonnes = self.get_colonnes_pertinentes()

        df = self.get_dataFrame().describe(self.colonnes_pertinentes)

        nb_total_lignes = self.count_row()
        ligne_summary = df.select(self.colonnes_pertinentes).where('summary = "count"')
        informationsManquantes = []
        for col in ligne_summary.columns:
            nombreLignesManquantes = nb_total_lignes - int(ligne_summary.select(col).collect()[0][0])
            if nombreLignesManquantes > 0:
                informationsManquantes.append(col)

        requeteSQL = ""
        identifiants_lignes_infosManque = []
        if len(informationsManquantes) > 0:
            requeteSQL = """SELECT distinct identifiant FROM {VUE}""".format(VUE=self.nom_vue)
            WHERE = " WHERE "
            for i, col in enumerate(informationsManquantes):
                WHERE += " {COL} is null ".format(COL=col)
                if i+1 != len(informationsManquantes):
                    WHERE += " OR "
            requeteSQL += WHERE
            df = self.spark.sql(requeteSQL)
            identifiants_lignes_infosManque = [ligne["identifiant"] for ligne in df.rdd.collect()]

        self.set_lignes_incompletes(identifiants_lignes_infosManque)

    def find_chantiers_termines_avant_le(self, date_debut_projet):
        requeteSQL = """SELECT distinct identifiant
                         FROM {VUE}
                         WHERE
                             to_date(substring(date_fin,0,10),"yyyy-MM-dd")
                                < to_date("{DATE_DEBUT_PROJET}","yyyy-MM-dd")
                      """.format(VUE=self.nom_vue, DATE_DEBUT_PROJET=date_debut_projet)

        df = self.spark.sql(requeteSQL)
        chantiersTermines = [ligne["identifiant"] for ligne in df.rdd.collect()]
        return chantiersTermines

    def set_lignes_incompletes(self, identifiants_lignes_infosManquantes):
        self.identifiants_lignes_infosManquantes = identifiants_lignes_infosManquantes

    def get_lignes_incompletes(self):
        return self.identifiants_lignes_infosManquantes

    def set_plan_travail_nettoyage(self, liste_identifiants=[],  type_action="delete"):
        """Nettoyage"""
        if len(liste_identifiants) == 0:
            self.plan_travail_nettoyage = {"delete": []}

        for identifiant in liste_identifiants:
            if identifiant not in self.plan_travail_nettoyage[type_action]:
                self.plan_travail_nettoyage[type_action].append(identifiant)

    def get_plan_travail_nettoyage(self, liste_identifiants=[], type_action="delete"):
        return self.plan_travail_nettoyage[type_action]

    def nettoyer(self, type_action="delete"):
        liste_a_supprimer = self.get_plan_travail_nettoyage()
        df = self.get_dataFrame()
        # CHECK THIS
        df_nettoyee = df.filter(~df['identifiant'].isin(liste_a_supprimer))
        self.set_dataFrame(df_nettoyee)

    #  méthodes générales sur les dataframe

    def count_row(self):
        df = self.get_dataFrame()
        return df.count()

    def covariance(self, colonne1, colonne2):
        if not self.colonnes_exist(colonne1):
            print("la colonne {} n'existe pas".format(colonne))
            return None
        if self.get_type_colonne(colonne1) == 'string':
            print("Calcul covariance impossible, la colonne {} est de type String".format(colonne1))
            return None
        if self.get_type_colonne(colonne2) == 'string':
            print("Calcul covariance impossible, la colonne {} est de type String".format(colonne2))
            return None
        df = self.get_dataFrame()
        print(df.corr(colonne1, colonne2))

    def get_type_colonne(self, colonne):
        if not self.colonnes_exist(colonne):
            print("la colonne {} n'existe pas".format(colonne))
            return None
        df = self.get_dataFrame()
        return [dtype for name, dtype in df.dtypes if name == colonne][0]

    def colonnes_exist(self, colonnes):
        myColonnes = []
        existe = True
        if type(colonnes) in [str, int, float]:
            myColonnes.append(str(colonnes))

        if type(colonnes) == list:
            myColonnes = colonnes

        df = self.get_dataFrame()
        for col in myColonnes:
            if col not in df.columns:
                print("La colonne {} n'exite pas dans la dataframe chantier".format(col))
                print("Liste des colonnes possibles :", self.liste_colonnes())
                existe = False
        return existe

    def liste_colonnes(self):
        df = self.get_dataFrame()
        return df.columns

    def clean(self):
        # Construire la vue brute des chantiers : chantier_data_brutes
        nomVueBrute = self.construireViewBruteChantier()

        # Selection des attributs pertinentes au regard de l'objectif du projet

        colonnesPertinentes = ["date_debut", "date_fin", "niveau_perturbation",
                               "impact_circulation", "geo_shape_json"]
        self.set_colonnes_pertinentes(colonnesPertinentes)

        #  Sythèse des informations pertinentes
        identifiants_lignes_infosManquantes = self.display_synthese_infos_manquantes()

        # Liste des chantiers terminés avant le 01/01/2014
        liste_chantiers_termines = self.find_chantiers_termines_avant_le("2014-01-01")

        #  Après analyse, les chantiers avec des informations pertinentes
        #  manquantes ou terminés doivent être supprimes. Ils sont donc ajoutés au plan de nettoyage

        self.set_plan_travail_nettoyage(liste_identifiants=self.get_lignes_incompletes(),
                                        type_action="delete")

        self.set_plan_travail_nettoyage(liste_identifiants=liste_chantiers_termines,
                                        type_action="delete")

        # self.get_plan_travail_nettoyage()

        # En partant du plan de travail, nettoyer les données
        self.nettoyer()

        # Sélectionner l'échantillon des CHANTIERS étudiés
        voies_echantillon = Echantillion_rue().conversion(self.get_voies())

        self.selection_echantillon(voies_echantillon)
        df_chantier = self.get_geoshape()
        return df_chantier
