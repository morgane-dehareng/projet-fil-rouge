FROM continuumio/miniconda3

RUN conda install -y requests pyspark openjdk=8  matplotlib seaborn scikit-learn
RUN conda install -y --channel conda-forge geopandas

WORKDIR /usr/src/app/
COPY . /usr/src/app/
ENV PYTHONPATH=$PYTHONPATH:/usr/src/app/datalake/src/:/usr/src/app/nettoyage/src/:/usr/src/app/learning/src/
ENTRYPOINT ["./trafic_paris.py"]
