#!/opt/conda/bin/python
# -*- coding: utf-8 -*-
"""
Definition de la classe pour la gestion de Saprk et l'interface avec hdfs
"""

from pyspark.sql import SparkSession


class managehdfs():

    def __init__(self, hdfs_path="hdfs://pfr-cloudera:8020"):
        self.set_hdfs_path(hdfs_path)
        self.init_spark()

    def set_hdfs_path(self, path):
        """ HDFS Path Setter
        """
        self.hdfs_path = path

    def init_spark(self):
        """ Creating spark session and context
        """
        self.spark = SparkSession.builder.appName('clean')\
            .master('local[6]')\
            .config("spark.driver.memory", "15g")\
            .config("spark.executor.memory", "15g")\
            .getOrCreate()
        self.sc = self.spark.sparkContext
        self.sc.setLogLevel("ERROR")
        self.spark.catalog.clearCache()

    def close_spark(self):
        """ Close context et clear cache
        """
        self.spark.catalog.clearCache()
        self.sc.stop()

    def read_hdfs(self, path):
        df = self.spark.read.parquet(self.hdfs_path+path)
        return df

    def write_hdfs(self, df, path):
        df.write.parquet(self.hdfs_path+path)
        return df
