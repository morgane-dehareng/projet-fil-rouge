#!/opt/conda/bin/python
# -*- coding: utf-8 -*-
from models import Models
import datetime


def learning():
    start = datetime.datetime.now()
    print("STARTED AT", start)
    arc_dic = {4962: 'Tolbiac', 5009: 'Tolbiac', 4965: 'Tolbiac', 5004: 'Tolbiac', 22: 'Rivoli',
               27: 'Rivoli', 30: 'Rivoli', 29: 'Rivoli', 370: 'Rivoli', 21: 'Rivoli', 279: 'Rivoli',
               4189: 'Rivoli', 35: 'Rivoli', 33: 'Rivoli', 31: 'Rivoli', 28: 'Rivoli', 25: 'Rivoli',
               283: 'Rivoli', 281: 'Rivoli', 176: 'Rivoli', 371: 'Rivoli', 282: 'Rivoli',
               4908: 'La_Fayette', 4906: 'La_Fayette', 6522: 'Belleville', 6280: 'Bd_Voltaire',
               1344: 'Bd_Voltaire', 1347: 'Bd_Voltaire', 1346: 'Bd_Voltaire', 1348: 'Bd_Voltaire',
               6277: 'Bd_Voltaire', 4737: 'Bd_Voltaire', 6276: 'Bd_Voltaire', 1358: 'Bd_Voltaire',
               4738: 'Bd_Voltaire', 4735: 'Bd_Voltaire', 6282: 'Bd_Voltaire', 6359: 'Bd_Voltaire',
               1349: 'Bd_Voltaire', 6358: 'Bd_Voltaire', 1359: 'Bd_Voltaire', 1345: 'Bd_Voltaire',
               6281: 'Bd_Voltaire', 5810: 'Bd_Raspail', 5849: 'Bd_Raspail', 5809: 'Bd_Raspail',
               5853: 'Bd_Raspail', 5805: 'Bd_Raspail', 5813: 'Bd_Raspail', 5807: 'Bd_Raspail',
               655: 'Bd_Raspail', 5811: 'Bd_Raspail', 6074: 'Bd_Malesherbes', 237: 'Bd_Malesherbes',
               1572: 'Bd_Magenta', 4229: 'Bd_Haussmann', 483: 'Bd_Diderot',
               486: 'Bd_Diderot', 1322: 'Av_Parmentier'}

    rues = ['Bd_Malesherbes', 'Av_Parmentier', 'Bd_Diderot', 'Bd_Haussmann', 'Tolbiac']

    for rue in rues:
        arcs = [k for k, v in arc_dic.items() if v == rue]
        n_proc = 0
        for arc in arcs:
            print(rue, arc)
            Dummy = Models(file='/user/root/Nettoye',
                           type='Dummy',
                           filter={'libelle': rue, 'id_arc': arc})
            Dummy.evaluation()

            RanFor = Models(file='/user/root/Nettoye',
                            type='RF',
                            filter={'libelle': rue, 'id_arc': arc})
            RanFor.evaluation()
            RanFor.savemodel()
            RanFor.visualize()
            # RanFor.close_spark()

            LogReg = Models(file='/user/root/Nettoye',
                            type='LR',
                            filter={'libelle': rue, 'id_arc': arc})
            LogReg.evaluation()
            LogReg.savemodel()
            LogReg.visualize()

            LogReg.close_spark()

            n_proc += 1
            if n_proc > 2:
                break

    print('ML ran in', datetime.datetime.now()-start)


if __name__ == "__main__":
    learning()
