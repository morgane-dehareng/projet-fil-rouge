#!/opt/conda/bin/python
# -*- coding: utf-8 -*-
import numpy as np

import pyspark.sql.functions as F
from pyspark.sql import types as T
from pyspark.ml import Pipeline
from pyspark.ml.linalg import DenseVector
from pyspark.ml.classification import RandomForestClassifier, RandomForestClassificationModel, LogisticRegression
from pyspark.ml.feature import StringIndexer, OneHotEncoder, VectorAssembler
from pyspark.ml.tuning import ParamGridBuilder, CrossValidator
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.mllib.evaluation import MulticlassMetrics

from sklearn.dummy import DummyClassifier
from sklearn.metrics import f1_score
import pandas as pd

from managehdfs import managehdfs

import datetime


class Models(managehdfs):

    def __init__(self, file="", hdfs_path="hdfs://pfr-cloudera:8020", filter={}, type='RF'):

        self.type = type
        if self.type == 'RF':
            print("RANDOM FOREST")
        elif self.type == "LR":
            print("LOGISTIC REGRESSION")
        elif self.type == "Dummy":
            print("DUMMY CLASSIFER")
        self.set_hdfs_path(hdfs_path)
        self.init_spark()
        if len(file) != 0:
            self.set_dataFrame(self.read_hdfs(file))
        self.filter = filter

        now = datetime.datetime.now()
        street = "_".join([str(x) for x in list(self.filter.values())])
        time = str(now.day)+'-'+str(now.hour+2)+'-'+str(now.minute)
        self.outn = '{}_{}_{}'.format(self.type, street, time)

    def set_filter(self, filter={}):
        self.filter = filter

    def set_dataFrame(self, df):
        self.data = df

    def get_dataFrame(self):
        return self.data

    def encoder_assembler(self, df, categoricalCols, continuousCols, labelCol):

        # StringIndexer encodes a string column of labels to a column of label indices.
        # Indices are in [0, numLabels)
        # Default “frequencyDesc”: descending order by label frequency (most frequent label -> 0)
        # => Ouvert = 0, Restreinte = 1, Sense unique = 2
        indexers = [StringIndexer(inputCol=c, outputCol="{0}_indexed".format(c))
                    for c in categoricalCols]

        # default setting: dropLast=True => last category excluded because obvious if first two = 0
        encoders = [OneHotEncoder(inputCol=indexer.getOutputCol(),
                    outputCol="{0}_encoded".format(indexer.getOutputCol()))
                    for indexer in indexers]

        # assemble categorical and numerical variables in one vector
        assembler = VectorAssembler(inputCols=[encoder.getOutputCol() for encoder in encoders]
                                    + continuousCols, outputCol="features")

        pipeline = Pipeline(stages=indexers + encoders + [assembler])

        model = pipeline.fit(df)
        data = model.transform(df)

        data = data.withColumn('label', F.col(labelCol))

        return data.select('features', 'label')

    def preparation(self):
        df = self.get_dataFrame()
        df = df.withColumn('temperature', df['temperature'].cast('float'))\
            .withColumn('nuage', df['nuage'].cast('float'))\
            .withColumn('visibilite', df['visibilite'].cast('float'))\
            .withColumn('precipitation', df['precipitation'].cast('float'))\
            .withColumn('niveau_perturbation',
                        df['niveau_perturbation'].cast('int'))\
            .withColumn('label', df['label'].cast('int'))

        # Only & filter is supported at the moment
        for key, value in self.filter.items():
            df = df.filter(df[key] == value)

        # list of different impact for the considered filters
        impact = [x[0] for x in df.groupBy('impact_circulation')
                                  .count()
                                  .sort('count', ascending=False)
                                  .collect()]
        if len(impact) > 0:
            categoricalColumns = ['impact_circulation']
        else:
            categoricalColumns = []

        numericCols = ['temperature', 'nuage', 'visibilite', 'precipitation',
                       'niveau_perturbation', 'heure', 'weekday', 'mois']

        self.feat_list = (['Circulation '+x.lower().replace("_", " ") for x in impact[:-1]]
                          + [x.title().replace("_", " ") for x in numericCols])

        traindummy = self.encoder_assembler(df, categoricalColumns, numericCols, 'label')

        # We add an ID to have a uniquely identified row for the subtraction later
        traindummy = traindummy.withColumn("id", F.monotonically_increasing_id())

        # create a dictionary with label distinct values as key and 80 % as value
        fractions = traindummy.select("label")\
                              .distinct()\
                              .withColumn("fraction", F.lit(0.8))\
                              .rdd.collectAsMap()

        seed = traindummy.count()
        # Sample the dataframe with the appropiate fractions to get train dataframe
        train_df = traindummy.stat.sampleBy("label", fractions, seed)
        train_count = train_df.count()

        # Subtraction to get validation dataframe, it takes a while...
        validation = traindummy.subtract(train_df)
        validation_count = validation.count()

        # This should be 0
        if validation_count+train_count-seed != 0:
            raise Error("Data split in train/test has not worked")
        train_df = train_df.drop('id')
        validation = validation.drop('id')

        return train_df, validation

    def random_forest(self):

        rf = RandomForestClassifier(labelCol="label", featuresCol="features")
        pipeline = Pipeline(stages=[rf])
        paramGrid = ParamGridBuilder()\
            .addGrid(rf.numTrees, [int(x) for x in np.linspace(start=10, stop=100, num=5)])\
            .addGrid(rf.maxDepth, [int(x) for x in np.linspace(start=10, stop=30, num=5)]) \
            .build()

        crossval = CrossValidator(estimator=pipeline,
                                  estimatorParamMaps=paramGrid,
                                  evaluator=MulticlassClassificationEvaluator(),
                                  numFolds=3)

        return crossval

    def logistic_regression(self):

        lr = LogisticRegression(featuresCol='features',
                                labelCol='label',
                                weightCol="classWeights",
                                family='multinomial')
        pipeline = Pipeline(stages=[lr])
        paramGrid = ParamGridBuilder()\
            .addGrid(lr.elasticNetParam, [0.0, 0.5, 1.0])\
            .addGrid(lr.regParam, [0., 0.01])\
            .build()

        crossval = CrossValidator(estimator=pipeline,
                                  estimatorParamMaps=paramGrid,
                                  evaluator=MulticlassClassificationEvaluator(),
                                  numFolds=3)

        return crossval

    def add_weight(self, train):

        tot = train.count()

        weight4 = train.select("label").where('label == 1').count()/tot/2.
        weight1 = train.select("label").where('label == 4').count()/tot
        weight2 = train.select("label").where('label == 3').count()/tot
        weight3 = train.select("label").where('label == 2').count()/tot

        trainw = train.withColumn("classWeights",
                                  F.when(train.label == 1, weight1)
                                  .when(train.label == 2, weight2)
                                  .when(train.label == 3, weight3)
                                  .otherwise(weight4))

        return trainw

    def sklearn_prep(self, dataframe):

        def sparse_to_array(v):
            v = DenseVector(v)
            new_array = list([float(x) for x in v])
            return new_array

        y = dataframe.select('label').toPandas()['label']

        sparse_to_array_udf = F.udf(sparse_to_array, T.ArrayType(T.FloatType()))

        df = dataframe.select('features')\
                      .withColumn('features', sparse_to_array_udf('features'))\
                      .toPandas()

        df = pd.DataFrame([x for x in df['features']],
                          columns=[x.replace(" ", "_") for x in self.feat_list])

        return df, y

    def training(self):

        train, valid = self.preparation()
        if self.type == 'RF':
            model = self.random_forest()
            cvModel = model.fit(train)
            bestPipeline = cvModel.bestModel
            self.model = bestPipeline.stages[0]
            print('numTrees - ', self.model.getNumTrees)
            print('maxDepth - ', self.model.getOrDefault('maxDepth'))
            return self.model, train, valid
        elif self.type == 'Dummy':
            self.model = DummyClassifier(strategy="stratified")
            X, y = self.sklearn_prep(train)
            X_val, y_val = self.sklearn_prep(valid)

            self.model.fit(X, y)

            return self.model, (X, y), (X_val, y_val)

        elif self.type == 'LR':
            trainw = self.add_weight(train)
            model = self.logistic_regression()
            cvModel = model.fit(trainw)
            bestPipeline = cvModel.bestModel
            self.model = bestPipeline.stages[0]
            # Print the coefficients and intercept for logistic regression
            print("Coefficients: " + str(self.model.coefficientMatrix))
            print("Intercept: " + str(self.model.interceptVector))
            return self.model, train, valid

    def evaluation(self):

        model, train, validation = self.training()

        if self.type == 'RF' or self.type == "LR":
            predictions = model.transform(validation)
            # Select (prediction, true label) and compute test error
            # F1 score A measurement that considers both precision and recall to compute the score.
            # It can be interpreted as a weighted average of the precision and recall values
            # F1 score reaches its best value at 1 and worst value at 0
            evaluator = MulticlassClassificationEvaluator(
                        labelCol="label", predictionCol="prediction", metricName="f1")
            f1 = evaluator.evaluate(predictions)
            f1unweight = (evaluator.evaluate(predictions[predictions['label'] == 1]) +
                          evaluator.evaluate(predictions[predictions['label'] == 2]) +
                          evaluator.evaluate(predictions[predictions['label'] == 3]) +
                          evaluator.evaluate(predictions[predictions['label'] == 4]))/4.
            print("Unweighted F1 score = %g" % (f1unweight))
            print("Weighted F1 score = %g" % (f1))

            predictionAndLabels = (predictions.withColumn('label',
                                                          predictions['label'].cast('double'))
                                   .select('prediction', 'label')
                                   .rdd)
            self.metrics = MulticlassMetrics(predictionAndLabels)

            conf = self.metrics.confusionMatrix().toArray()
            print("Cnfusion matrix: ", conf)

            return self.metrics

        elif self.type == 'Dummy':
            y_pred = model.predict(validation[0])

            f1unweight = f1_score(validation[1], y_pred, average='macro')
            f1 = f1_score(validation[1], y_pred, average='weighted')
            print("Unweighted F1 score = %g" % (f1unweight))
            print("Weighted F1 score = %g" % (f1))

    def savemodel(self, dir="/user/root/Models/"):

        modelFile = '{}{}'.format(dir, self.outn)
        self.model.save(self.hdfs_path+modelFile)

    def loadmodel(self, path="./"):
        if self.type != 'RF':
            raise ('Error, only RF model can be loaded')

        self.model = RandomForestClassificationModel.load(path)

    def visualize(self):

        import matplotlib.pyplot as plt
        import seaborn as sns

        sns.set_style('white')

        if self.type == 'RF':
            importances = self.model.featureImportances
            print(self.feat_list, importances)

            x_values = list(range(len(importances)))

            fig, ax1 = plt.subplots()

            ax1.bar(x_values, importances, orientation='vertical')
            ax1.set_xticks(range(len(self.feat_list)))
            ax1.set_xticklabels(self.feat_list, rotation=90)
            ax1.set_ylabel('Importance')

            ax1.set_title('Feature Importances - rue de {} arc {}'.format(self.filter['libelle'],
                                                                          self.filter['id_arc']))

            ax1.spines['right'].set_visible(False)
            ax1.spines['left'].set_alpha(0.3)
            ax1.spines['top'].set_visible(False)
            ax1.spines['bottom'].set_alpha(0.3)

            plt.tight_layout()
            plt.savefig('/usr/src/app/Output/featureimportances_{}_{}_{}.png'.format(self.filter['libelle'],
                                                                 self.filter['id_arc'],
                                                                 self.type))

        conf = self.metrics.confusionMatrix().toArray()

        label_name = ['Fluide', 'Pré-saturé', 'Saturé', 'Bloqué']
        plt.figure()
        ax1 = sns.heatmap(conf, annot=True, fmt="g")
        ax1.set_xlabel('Estimée')
        ax1.set_ylabel('Réelle')
        ax1.set_title('Confusion Matrix - rue de {}, Arc {}'.format(self.filter['libelle'],
                                                                    self.filter['id_arc']))

        ax1.set_xticklabels(label_name)
        ax1.set_yticklabels(label_name)

        plt.savefig('/usr/src/app/Output/confusion_{}_{}_{}.png'.format(self.filter['libelle'],
                                                    self.filter['id_arc'],
                                                    self.type))
