# PRÉVISION DE L'ÉTAT DU TRAFIC PARISIEN

## Description

Le trafic parisien se densifie de jour en jour et occasionne des désagréments voire des pertes financières. Dans ce contexte, il nous parait important de prédire l'état du trafic à une date et heure données.

## Prédictions

Prédire l'état du trafic parisien à partir des données issues de capteurs permanents et de la météo.

## Environnement

* Python
* Docker
* Hadoop
* Spark
* Tableau

## Sources des données

* Données trafic: [Paris Open Data](https://opendata.paris.fr/explore/dataset/comptages-routiers-permanents/).
* Jours fériés: [Data gouv](https://www.data.gouv.fr/en/datasets/jours-feries-en-france/)
* Météo: [Historique Meteo](https://www.historique-meteo.net/france/ile-de-france/paris/), utilisant les données de [World Weather Online](https://www.worldweatheronline.com/)
* Chantiers perturbants la circulation : [Data Île-de-France](https://data.iledefrance.fr/explore/dataset/chantiers-perturbants-la-circulation).


Joelle Canarella, Paolo Cumani, Morgane Dehareng, Giresse Ngoko

Responsable: Yassine Elmaataoui
